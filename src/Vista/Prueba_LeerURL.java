/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import ufps.util.varios.ArchivoLeerURL;
/**
 *
 * @author madar
 */
public class Prueba_LeerURL {
    public static void main(String[] args) {
        ArchivoLeerURL archivo=new ArchivoLeerURL("https://gitlab.com/estructuras-de-datos-i-sem-2019/persistencia/coordenascartesianas/-/raw/master/puntos.csv");
        //Obtiene toda la información del archivo en un vector de object
        Object datos[]=archivo.leerArchivo();
        int i=0;
        for(Object fila:datos)
            System.out.println("Fila ["+(i++)+"]:"+fila.toString());
    }
}
